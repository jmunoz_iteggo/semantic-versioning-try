# [1.10.0](https://bitbucket.org/jmunoz_iteggo/semantic-versioning-try/compare/v1.9.0...v1.10.0) (2021-06-30)


### Features

* **ci/cd:** prueba 13 ([b4c182f](https://bitbucket.org/jmunoz_iteggo/semantic-versioning-try/commits/b4c182fc488866322edc01b53bfecebb8b54c995))

# [1.9.0](https://bitbucket.org/jmunoz_iteggo/semantic-versioning-try/compare/v1.8.0...v1.9.0) (2021-06-30)


### Features

* **ci/cd:** prueba 12 ([57635b8](https://bitbucket.org/jmunoz_iteggo/semantic-versioning-try/commits/57635b8fae2086c0aa2d84bfbc4a5f8c9e79d34c))

# [1.8.0](https://bitbucket.org/jmunoz_iteggo/semantic-versioning-try/compare/v1.7.0...v1.8.0) (2021-06-30)


### Features

* **ci/cd:** prueba 11 ([2007cd3](https://bitbucket.org/jmunoz_iteggo/semantic-versioning-try/commits/2007cd3fa753c1a1578d357f7ed1b94e2a75f619))

# [1.7.0](https://bitbucket.org/jmunoz_iteggo/semantic-versioning-try/compare/v1.6.0...v1.7.0) (2021-06-30)


### Features

* **ci/cd:** prueba 10 ([7e77c13](https://bitbucket.org/jmunoz_iteggo/semantic-versioning-try/commits/7e77c133df70ce5c4c5750cc2b3a188e0affd812))

# [1.6.0](https://procde.prosegur.com/bitbucket/scm/~esx0002767/semantic-versioning-try/compare/v1.5.0...v1.6.0) (2021-06-30)


### Features

* **ci/cd:** prueba 9 ([31b9e96](https://procde.prosegur.com/bitbucket/scm/~esx0002767/semantic-versioning-try/commit/31b9e966b128e376a7363a79d4a3cb8a8a6fff84))

# [1.5.0](https://procde.prosegur.com/bitbucket/scm/~esx0002767/semantic-versioning-try/compare/v1.4.0...v1.5.0) (2021-06-30)


### Features

* **ci/cd:** prueba 6 ([b75426b](https://procde.prosegur.com/bitbucket/scm/~esx0002767/semantic-versioning-try/commit/b75426b1b8d19ea95eafa9e104b366ca1b1fcbb2))
* **ci/cd:** prueba 7 ([6b2383e](https://procde.prosegur.com/bitbucket/scm/~esx0002767/semantic-versioning-try/commit/6b2383e59875d0b379aec0760c6fb5e232608f66))
* **ci/cd:** prueba 8 ([69f9902](https://procde.prosegur.com/bitbucket/scm/~esx0002767/semantic-versioning-try/commit/69f9902537234fab7e05d28796bb62edd9ada1d8))
